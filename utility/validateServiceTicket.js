/**
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 */
/**
 *
 * @fileOverview This file exports the <i>validateServiceTicket</i> module that allows
 * authenticate module to validate ticket it got back after user signed in.
 */

const request = require('request');

/**
 * validate ticket from CAS login
 * @param servicePackage this package include full service url and full cas validate url and ticket
 * @return {Promise<any>}
 */
function validateServiceTicket(servicePackage) {
  const self = this;
  function promise(resolve, reject) {
    const reqOptions = {
      url: servicePackage.fullValidateUrl,
      qs: {
        ticket: servicePackage.ticket,
        service: servicePackage.fullServiceUrl,
      },
      strictSSL: self.strictSSL,
    };

    request(reqOptions, (error, response, body) => {
      if (error) {
        return reject(error);
      }

      if (response.statusCode !== 200) {
        return reject(new Error(
          `CAS server returned status: ${response.statusCode}`,
        ));
      }
      // parse xml result
      servicePackage.validate(body).then((msg) => {
        resolve(msg);
      }).catch((err) => {
        reject(err);
      });
    });
  }
  return new Promise(promise);
}

module.exports = validateServiceTicket;
