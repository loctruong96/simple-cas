# simple-cas

This module take inspiration from `simple-cas-interface` and `connect-cas` to
create a middleware for express that is easy to use as well as dynamic in term 
of url redirection so that complicated route can be correctly redirected.
`simple-cas` support basic authentication for CAS 1.0 to CAS 3.0. if none is 
declared in the configuration process, CAS 2.0 will be used.

CAS(Central Authentication Service) is a single-sign-on / single-sign-off protocol for the web.

We suppose you are already familiar with the CAS protocol, if not, please read this [document](https://apereo.github.io/cas/5.1.x/protocol/CAS-Protocol-Specification.html) before you use this.

## Install

    npm install simple-cas

## Feature

1. Non-proxy mode CAS login/logout
2. Single sign off

## Quick start

```javascript
const express = require('express');
const url = require('url');
const session = require('express-session');
const CAS = require('simple-cas');

CAS.configure({ host: 'your.cas.domain.host', service: 'http://localhost:3000/' });
const app = express();

// Use cookie sessions for simplicity, you can use something else
app.use(session({
  secret: 'supersecuretext',
  resave: false,
  saveUninitialized: true,
}));

app.get('/', (req, res) => {
  if (req.session.cas && req.session.cas.user) {
    return res.send(`<p>You are logged in. Your username is ${req.session.cas.user}. <a href="/logout">Log Out</a></p>`);
  }
  return res.send('<p>You are not logged in. <a href="/login">Log in now.</a><p>');
});

app.get('/login', CAS.authenticate(CAS.configure(), 'login'), (req, res) => {
  res.redirect('/');
});

// you can also set CAS configurations dynamically within other middleware using req.data object
// you can set req.data.host, and req.data.service and req.data.protocolVersion
// your settings will remains for every request afterward, and everywhere else in this app.
app.get('/login', (req, res, next) => {
  req.data = {};
  req.data.host = 'your.cas.domain.host';
  req.data.service = 'http://localhost:3000/';
  next();
},CAS.authenticate(CAS.configure(), 'login') ,(req, res) => {
  res.redirect('/');
});

app.get('/logout', (req, res) => {
  if (!req.session) {
    return res.redirect('/');
  }
  // Forget our own login session
  if (req.session.destroy) {
    req.session.destroy();
  } else {
    // Cookie-based sessions have no destroy()
    req.session = null;
  }
  // Send the user to the official campus-wide logout URL
  const options = CAS.configure();
  // set logout url
  options.pathname = options.paths.logout;
  // set return url after logout
  options.query = options.query || {};
  options.query.service = options.service;
  return res.redirect(url.format(options));
});

app.listen(3000);

```

## Configuration

To initialize `simple-cas` configurations. 
```javascript

const CAS = require('simple-cas');

CAS.configure({ host: 'your.cas.domain.host', service: 'http://localhost:3000/' });

```

`simple-cas` default configurations are as follow.

```javascript
const defaults = {
  protocol: 'https',
  host: undefined,
  port: 443,
  service: undefined,
  protocolVersion: 2.0, // default to 2.0
  validateTicketProtocol: protocol2, // default to 2.0 CAS ticket parsing
  paths: {
    serviceValidate: '/cas/serviceValidate', // default to 2.0 route
    login: '/cas/login',
    logout: '/cas/logout',
  },
};
```

## License
[MIT](https://opensource.org/licenses/MIT)