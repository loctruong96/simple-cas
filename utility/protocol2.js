/**
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 */
const xml2js = require('xml2js');
const XmlParser = require('./XmlParser');


const xmlParser = new XmlParser();

/**
 * Processes CAS protocol 2.0 result XML.
 *
 * @private
 * @param {string} xml The response to process.
 * @returns {Promise}
 */
function validate(xml) {
  return new Promise((resolve, reject) => {
    xml2js.parseString(
      xml,
      {
        explicitArray: false,
        tagNameProcessors: [name => (
          name.replace('cas:', '')
        )],
      },
      (err, jsxml) => {
        if (err) {
          return reject(err);
        }
        xmlParser.parse(jsxml)
          .then(result => resolve(result))
          .catch(er => reject(er));
      },
    );
  });
}

module.exports = validate;
