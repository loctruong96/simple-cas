/**
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 */

/**
 * Processes CAS protocol 1.0 result
 *
 * @private
 * @param {string} response The response to process.
 * @returns {Promise}
 */
function validate(response) {
  function promise(resolve, reject) {
    let msg;
    const parts = response.toLowerCase().split('\n');
    if (parts.length < 2) {
      msg = `Received invalid CAS 1.0 validation response: ${response}`;
      return reject(new Error(msg));
    }
    if (parts[0] === 'yes') {
      return resolve(true);
    } if (parts[0] === 'no') {
      msg = 'Received bad validation from CAS 1.0 server';
      return reject(new Error(msg));
    }
  }

  return new Promise(promise);
}

module.exports = validate;
