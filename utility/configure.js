/**
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 */
const _ = require('lodash');
const url = require('url');
const protocol2 = require('./protocol2');

const defaults = {
  protocol: 'https',
  host: undefined,
  port: 443,
  service: undefined,
  protocolVersion: 2.0, // default to 2.0
  validateTicketProtocol: protocol2, // default to 2.0 CAS ticket parsing
  paths: {
    serviceValidate: '/cas/serviceValidate', // default to 2.0 route
    login: '/cas/login',
    logout: '/cas/logout',
  },
};

module.exports = (options) => {
  if (!options) return _.cloneDeep(defaults);
  // ensure that service url is a valid url
  const data = _.extend(defaults, options);
  if (options.service) options.service = url.format(data.service);
  return _.extend(defaults, options);
};
